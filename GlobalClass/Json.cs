﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GlobalClass
{
    public class Json
    {
        /// <summary>
        ///  تبدیل کلاس به رشته جیسون
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Value"></param>
        /// <returns></returns>
        public async Task<string> ToJson<T>(T Value)
        {
            var Result = JsonConvert.SerializeObject(Value);

            return Result;
        }

        /// <summary>
        /// تبدیل به آبجکت
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public async static Task<T> ToObjet<T>(string Value)
        {
            var Result = JsonConvert.DeserializeObject<T>(Value);

            return Result;
        }
    }
}
