﻿using System;
using KarYabiAsan.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(KarYabiAsan.Areas.Identity.IdentityHostingStartup))]
namespace KarYabiAsan.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
        //public void Configure(IWebHostBuilder builder)
        //{
        //    builder.ConfigureServices((context, services) => {
        //        services.AddDbContext<KarYabiAsanContext>(options =>
        //            options.UseSqlServer(
        //                context.Configuration.GetConnectionString("KarYabiAsanContextConnection")));

        //        services.AddDefaultIdentity<IdentityUser>(
        //            options => {
        //                //options.SignIn.RequireConfirmedAccount = true;
        //                //options.Password.RequireDigit = false;
        //                //options.Password.RequireLowercase = false;
        //                //options.Password.RequiredLength = 3;
        //                //options.Password.RequireNonAlphanumeric= false;
        //                //options.Password.RequireUppercase= false;
        //                //options.SignIn.RequireConfirmedEmail = false;
        //                //options.SignIn.RequireConfirmedPhoneNumber = true;
        //                //options.SignIn.RequireConfirmedAccount = false;
        //                //options.User.AllowedUserNameCharacters = null;
        //                options.SignIn.RequireConfirmedEmail = false;
        //                options.SignIn.RequireConfirmedPhoneNumber = true;
        //                options.SignIn.RequireConfirmedAccount = true;
        //                options.Password.RequiredLength = 6;
        //                options.Password.RequiredUniqueChars = 6;
        //                options.Password.RequireLowercase = false;
        //                options.Password.RequireNonAlphanumeric = false;
        //                options.Password.RequireUppercase = false;
        //                options.User.AllowedUserNameCharacters = null;
        //            }


        //            )
        //            .AddEntityFrameworkStores<KarYabiAsanContext>();
        //    });
        //}
    }
}