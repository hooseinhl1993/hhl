﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using KarYabiAsan.Models;
using System.Linq;

namespace KarYabiAsan.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ForgotPasswordModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IEmailSender _emailSender;
        private readonly KarYabiDBContext _context;
        public ForgotPasswordModel(KarYabiDBContext context, UserManager<IdentityUser> userManager, IEmailSender emailSender)
        {
            _userManager = userManager;
            _emailSender = emailSender;
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [Display(Name = "شماره تماس خود را وارد کنید")]
            [Phone]
            public string Mobile { get; set; }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(Input.Mobile);
                if (user == null )
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToPage("./Register");
                }
                if (!(await _userManager.IsPhoneNumberConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToPage("./ForgotPasswordConfirmation",new { Mobile=Input.Mobile });
                }

                //// For more information on how to enable account confirmation and password reset please 
                //// visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                //code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                //var callbackUrl = Url.Page(
                //    "/Account/ResetPassword",
                //    pageHandler: null,
                //    values: new { area = "Identity", code },
                //    protocol: Request.Scheme);

                //await _emailSender.SendEmailAsync(
                //    Input.Mobile,
                //    "Reset Password",
                //    $"Please reset your password by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
                if (!await SendVerifyCode(Input.Mobile, user.Id))
                {
                    ModelState.AddModelError("ExpireCode", "تعداد دفعات تست بیش از 5 بار بوده بعد از 5 دقیقه دوباره تلاش کنید");
                    return Page();
                }
                return RedirectToPage("./ResetPassword",new {Mobile= Input.Mobile,Token =code});
            }

            return Page();
        }

        public async Task<bool> SendVerifyCode(string PhoneNumber, string UserId)
        {
            bool Sended = _context.UsersVerifies.Where(p => p.PhoneNumber == PhoneNumber && p.dateTime>DateTime.Now.AddMinutes(-5)).ToList().Count() > 5?false:true;
            if (Sended)
            {
                Random random = new Random();
                int value = random.Next(1001, 9895);
                var Verify = new UsersVerifyMessage()
                {
                    PhoneNumber = PhoneNumber,
                    Code = value,
                    dateTime = DateTime.Now
                };
                _context.UsersVerifies.Add(Verify);
                await _context.SaveChangesAsync();
                await SMS.KasBarg.SendCodeVerify(UserId, PhoneNumber, value + "");
                return Sended;
            }
            else
            {
                return false;
            }
        }
    }
}
