﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using KarYabiAsan.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace KarYabiAsan.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ForgotPasswordConfirmation : PageModel
    {
        private readonly KarYabiDBContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        [BindProperty]
        public InputModel Input { get; set; }
        public ForgotPasswordConfirmation(KarYabiDBContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public class InputModel
        {

            public string PhoneNumber { get; set; }

            [Required]
            [Display(Name = "شماره تماس خود را وارد کنید")]
            [Phone]
            public string Code { get; set; }
        }

        public async Task<IActionResult> OnGet(string Mobile = "")
        {
            var user = await _userManager.FindByEmailAsync(Mobile);
            Input = new InputModel
            {
                PhoneNumber = Mobile
            };

            if (!await SendVerifyCode(Mobile, user.Id))
            {
                ModelState.AddModelError("ExpireCode", "تعداد دفعات تست بیش از 5 بار بوده بعد از 5 دقیقه دوباره تلاش کنید");
            }
            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(Input.PhoneNumber);
                var isvalid = _context.UsersVerifies.Where(p => p.PhoneNumber == Input.PhoneNumber && (DateTime.Now.Minute - p.dateTime.Minute) <= 2).ToList().Count() > 0 ? true : false;

                if (isvalid)
                {
                    user.PhoneNumberConfirmed = true;
                    await _userManager.UpdateAsync(user);
                    await _context.SaveChangesAsync();
                    return RedirectToPage("./ForgotPassword");
                }
                else
                {
                    await SendVerifyCode(Input.PhoneNumber, user.Id);
                    ModelState.AddModelError("ExpireCode", "این کد منقضی شده است");
                    return RedirectToAction(nameof(OnGet), new { Mobile = Input.PhoneNumber });
                }

            }

            return Page();
        }

        public async Task<bool> SendVerifyCode(string PhoneNumber, string UserId)
        {
            bool Sended = _context.UsersVerifies.Where(p => p.PhoneNumber == PhoneNumber && p.dateTime > DateTime.Now.AddMinutes(-5)).ToList().Count() > 5 ? false : true;
            if (Sended)
            {
                Random random = new Random();
                int value = random.Next(1001, 9895);
                var Verify = new UsersVerifyMessage()
                {
                    PhoneNumber = PhoneNumber,
                    Code = value,
                    dateTime = DateTime.Now
                };
                _context.UsersVerifies.Add(Verify);
                await _context.SaveChangesAsync();
                await SMS.KasBarg.SendCodeVerify(UserId, PhoneNumber, value + "");
                return Sended;
            }
            else
            {
                return false;
            }
        }
    }
}
