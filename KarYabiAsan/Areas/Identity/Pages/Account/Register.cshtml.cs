﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using GlobalClass;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace KarYabiAsan.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly Models.KarYabiDBContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        public RegisterModel(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
             Models.KarYabiDBContext context, 
             RoleManager<IdentityRole> roleManager
            )
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            //[Required]
            //[EmailAddress]
            //[Display(Name = "آدرس ایمیل")]
            //public string Email { get; set; }
            [Required]
            [Display(Name = "شماره تلفن همراه خود را وارد کنید")]
            [Phone]
            public string Mobile { get; set; }

            [Required]
            //[StringLength(100, ErrorMessage = "رمز عبور باید حداقل 6 کاراکتر باشد", MinimumLength = 6)]
            //[DataType(DataType.Password)]
            [Display(Name = "رمز عبور")]
            public string Password { get; set; }
            [Required]
            [Display(Name = "نام خود را وارد کنید")]
            public string Name { get; set; }
            //[Required]
            //[Display(Name = "نام خانوادگی خود را وارد کنید")]
            //public string Family { get; set; }
            //[DataType(DataType.Password)]
            [Display(Name = "تکرار رمز عبور")]
            [Compare("Password", ErrorMessage = "رمز عبور و تکرار آن مطابقت ندارند")]
            public string ConfirmPassword { get; set; }
        }




        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = Input.Mobile, Email = Input.Mobile };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    string RandomCode = "KarYAbi-" + RandomString(8);
                    while (_context.Jobsearchers.Where(p => p.UinqCode.Contains(RandomCode)).ToList().Count > 0)
                    {
                        RandomCode = "KarYAbi-" + RandomString(8);
                    }
                    _context.Jobsearchers.Add(new Models.jobsearcher() { UserID = user.Id,Email="", Token = user.Id, Phone = Input.Mobile, Name = Input.Name,UinqCode= RandomCode });

                    bool adminRoleExists = await _roleManager.RoleExistsAsync("JobSearcher");
                    if (!adminRoleExists)
                    {
                        await _roleManager.CreateAsync(new IdentityRole("JobSearcher"));
                    }

                    if (result.Succeeded)
                    {
                        // assign an existing role to the newly created user
                        await _userManager.AddToRoleAsync(user, "JobSearcher");
                    }
                    _context.SaveChanges();

                    //_logger.LogInformation("User created a new account with password.");

                    //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    //var callbackUrl = Url.Page(
                    //    "/Account/ConfirmEmail",
                    //    pageHandler: null,
                    //    values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                    //    protocol: Request.Scheme);

                    //await _emailSender.SendEmailAsync(Input.Mobile, "Confirm your email",
                    //    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    //if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    //{
                    //    return RedirectToPage("RegisterConfirmation", new { email = Input.Mobile, returnUrl = returnUrl });
                    //}
                    //else
                    //{

                    await _signInManager.SignInAsync(user, isPersistent: false);
                    returnUrl += "Users/UserProfile";
                    return RedirectToAction("Details", "jobsearchers",new { Un = AESEncryption.EncryptData(Input.Mobile, AESEncryption.KeyTelma) ,Ps= AESEncryption.EncryptData(Input.Password, AESEncryption.KeyTelma) });
                    //}
                }
                foreach (var error in result.Errors)
                {
                    if (error.Code== "DuplicateUserName")
                    {
                        ModelState.AddModelError(string.Empty, "با این شماره قبلا ثبت نام انجام شده است");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                   
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
