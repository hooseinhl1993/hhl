﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KarYabiAsan.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;

namespace KarYabiAsan.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ResetPasswordModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly KarYabiDBContext _context;
        public ResetPasswordModel(KarYabiDBContext context, UserManager<IdentityUser> userManager)
        {
            _userManager = userManager; _context = context;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [Display(Name = "شماره تماس خود را وارد کنید")]
            [Phone]
            public string Mobile { get; set; }

            [Required]
            [Display(Name = "رمز عبور")]
            [DataType(DataType.Password)]
            public string Password { get; set; }
          
            [Display(Name = "تایید رمز عبور")]
            [Compare("Password", ErrorMessage = "رمز عبور و تاییدیه مطابقت ندارد")]
            public string ConfirmPassword { get; set; }

            public string Code { get; set; }
            public string Token { get; set; }
        }

        public IActionResult OnGet(string Mobile = null,string Token=null)
        {
            Input = new InputModel
            {
                Mobile = Mobile,
                Token= Token
            };
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var isvalid = _context.UsersVerifies.Where(p => p.PhoneNumber == Input.Mobile && p.dateTime > DateTime.Now.AddMinutes(-2)).ToList().Count() > 0 ? true : false;
            var user = await _userManager.FindByEmailAsync(Input.Mobile);
            if (isvalid)
            {
                
                if (user == null)
                {
                    // Don't reveal that the user does not exist
                    return RedirectToAction("LogIn", "jobsearchers");
                }
                var result = await _userManager.ResetPasswordAsync(user, Input.Token, Input.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("LogIn", "jobsearchers");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
               
            }
            else
            {
                await SendVerifyCode(Input.Mobile, user.Id);
                ModelState.AddModelError("ExpireCode", "این کد منقضی شده است");
                return RedirectToAction(nameof(OnGet), new { Mobile = Input.Mobile ,Token=Input.Token});
            }
            return Page();
        }

        public async Task<bool> SendVerifyCode(string PhoneNumber, string UserId)
        {
            bool Sended = _context.UsersVerifies.Where(p => p.PhoneNumber == PhoneNumber && p.dateTime > DateTime.Now.AddMinutes(-5)).ToList().Count() > 5 ? false : true;
            if (Sended)
            {
                Random random = new Random();
                int value = random.Next(1001, 9895);
                var Verify = new UsersVerifyMessage()
                {
                    PhoneNumber = PhoneNumber,
                    Code = value,
                    dateTime = DateTime.Now
                };
                _context.UsersVerifies.Add(Verify);
                await _context.SaveChangesAsync();
                await SMS.KasBarg.SendCodeVerify(UserId, PhoneNumber, value + "");
                return Sended;
            }
            else
            {
                return false;
            }
        }
    }
}
