﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using KarYabiAsan.Models;
using KarYabiAsan.Models.ViewModels;
using KarYabiAsan.Areas.WebService.Models;
using Microsoft.AspNetCore.Identity;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using QRCoder;
using System.Drawing;

namespace KarYabiAsan.Areas.WebService
{
    [Route("api/[controller]")]
    [ApiController]
    public class TelmaJobWebService : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly KarYabiDBContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private IHostingEnvironment _env;
        public TelmaJobWebService(KarYabiDBContext context, IHostingEnvironment env)
        {
            _context = context; 
            _env = env;
        }
        [HttpGet("FirstPage")]
        public async Task<ActionResult<ApiFirstPage>> FirstPage(int Index, string SearchedText = "", AppSetting.ModeFirstPageTitle ModeTitle = AppSetting.ModeFirstPageTitle.Both)
        {
            List<ApiFirstPageTitles> JobList = await _context.Job.Where(p => p.Title.Contains(SearchedText)).OrderByDescending(p => p.JobId).Skip(Index * AppSetting.PageCount).Take(AppSetting.PageCount).Select(p => new ApiFirstPageTitles() { Id = p.JobId, ModeTitle = AppSetting.ModeFirstPageTitle.Job, Name = p.CompanyName, Title = p.Title }).ToListAsync();
            List<ApiFirstPageTitles> RezomeList = await _context.Rezomes.Where(p => p.OnvanShoghl.Contains(SearchedText)).OrderByDescending(p => p.Id).Skip(Index * AppSetting.PageCount).Take(AppSetting.PageCount).Select(p => new ApiFirstPageTitles() { Id = p.Id, ModeTitle = AppSetting.ModeFirstPageTitle.Rezome, Name = p.OnvanShoghl, Title = p.Skills }).ToListAsync();
            ApiFirstPage firstPages = new ApiFirstPage();
            switch (ModeTitle)
            {
                case AppSetting.ModeFirstPageTitle.Job:
                    firstPages.Titles.AddRange(JobList);
                    break;
                case AppSetting.ModeFirstPageTitle.Rezome:
                    firstPages.Titles.AddRange(RezomeList);
                    break;
                case AppSetting.ModeFirstPageTitle.Both:
                    firstPages.Titles.AddRange(JobList);
                    firstPages.Titles.AddRange(RezomeList);
                    break;
            }
            ////آخرن کار های امروز
            //firestPageViewModel.Jobsearchers = await _context.Jobsearchers.OrderByDescending(p => p.Id).Take(16).ToListAsync();
            //ViewBag.EmployerCount = _context.Employer.Count();
            //ViewBag.JobResearcherCount = _context.Jobsearchers.Count();
            //ViewBag.RezomesCount = _context.Rezomes.Count();
            //ViewBag.JobCount = _context.Job.Count();
            //ViewBag.JobCategoryList = await _context.JobCategories.ToListAsync();
            return (firstPages);
        }

        [HttpGet("TitleDetail")]
        public async Task<ActionResult<Object>> TitleDetail(int Id, AppSetting.ModeFirstPageTitle Mode)
        {
            if (Mode == AppSetting.ModeFirstPageTitle.Job)
            {
                var Job = await _context.Job.Where(p => p.JobId == Id).FirstOrDefaultAsync();
                return Job;
            }
            else
            {
                var Rezome = await _context.Rezomes.Where(p => p.Id == Id).FirstOrDefaultAsync();
                return Rezome;
            }
        }


        #region JobSearcher
        [HttpGet("RegisterJobSearcher")]
        public async Task<ActionResult<jobsearcher>> RegisterJobSearcher(string Name, string Mobile, string Password)
        {
            var user = new IdentityUser { UserName = Mobile, Email = Mobile };
            var result = await _userManager.CreateAsync(user, Password);
            if (result.Succeeded)
            {
                string RandomCode = "KarYAbi-" + RandomString(8);
                while (_context.Jobsearchers.Where(p => p.UinqCode.Contains(RandomCode)).ToList().Count > 0)
                {
                    RandomCode = "KarYAbi-" + RandomString(8);
                }
                var JobSearcher = new jobsearcher() { UserID = user.Id, Email = "", Token = user.Id, Phone = Mobile, Name = Name, UinqCode = RandomCode };
                _context.Jobsearchers.Add(JobSearcher);

                bool adminRoleExists = await _roleManager.RoleExistsAsync("JobSearcher");
                if (!adminRoleExists)
                {
                    await _roleManager.CreateAsync(new IdentityRole("JobSearcher"));
                }

                if (result.Succeeded)
                {
                    // assign an existing role to the newly created user
                    await _userManager.AddToRoleAsync(user, "JobSearcher");
                }
                _context.SaveChanges();
                return JobSearcher;
            }
            return null;
        }

        [HttpGet("LogInJobSearcher")]
        public async Task<ActionResult<jobsearcher>> LogInJobSearcher(string Mobile, string Password)
        {
            var result = await _signInManager.PasswordSignInAsync(Mobile, Password, true, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                var User =await _userManager.FindByEmailAsync(Mobile);
                var JobSearcher =await _context.Jobsearchers.Where(p => p.UserID == User.Id).FirstOrDefaultAsync();
                return JobSearcher;
            }
            else
            {
                return null;
            }
        }

        [HttpGet("EditeJobSearcher")]
        public async Task<ActionResult<jobsearcher>> EditeJobSearcher(jobsearcher jobsearcher)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("ExpireCode", "اطلاعات را به درستی وارد کنید");
                return null;
            }

            _context.Jobsearchers.Update(jobsearcher);
            _context.SaveChanges();
            return jobsearcher;
        }
        /// <summary>
        /// نشان کردن شغل 
        /// اگر جواب 0 بود تیک را بردار
        /// اگر 1 بود تیک بذار
        /// </summary>
        /// <param name="IdJob"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpGet("PinJob")]
        public async Task<ActionResult<int>> PinJob(int IdJob, string UserId)
        {
            var IsPinned = _context.BadgedJobs.Where(p => p.JobId == IdJob && p.UserId == UserId).FirstOrDefault();
            if (IsPinned != null)
            {
                _context.BadgedJobs.Remove(IsPinned);
                _context.SaveChanges();
                return 0;
            }
            else
            {
                _context.BadgedJobs.Add(new BadgedJobs() { JobId = IdJob, UserId = UserId });
                _context.SaveChanges();
                return 1;
            }
        }

        /// <summary>
        /// ارسال رزومه 
        /// اگر جواب 0 بود تیک را بردار
        /// اگر 1 بود تیک بذار
        /// </summary>
        /// <param name="IdJob"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpGet("SendRezome")]
        public async Task<ActionResult<int>> SendRezome(int IdJob, string UserId)
        {
            var IsPinned = _context.SendRezomes.Where(p => p.JobId == IdJob && p.UserId == UserId).FirstOrDefault();
            if (IsPinned != null)
            {
                _context.SendRezomes.Remove(IsPinned);
                _context.SaveChanges();
                return 0;
            }
            else
            {
                _context.SendRezomes.Add(new SendRezome() { JobId = IdJob, UserId = UserId });
                _context.SaveChanges();
                return 1;
            }
        }

        /// <summary>
        /// تایید در خواست همکاری 
         /// اگر جواب 0 بود تیک را بردار
        /// اگر 1 بود تیک بذار
        /// </summary>
        /// <param name="IdRquest"></param>
        /// <returns></returns>
        [HttpGet("HamkariRequests")]
        public async Task<ActionResult<int>> HamkariRequests(int IdRquest)
        {
            var IsPinned = _context.HamkariRequests.Where(p => p.Id == IdRquest).FirstOrDefault();
            if (IsPinned.IsConfirmed)
            {
                IsPinned.IsConfirmed = false;
                _context.HamkariRequests.Update(IsPinned);
                _context.SaveChanges();
                return 0;
            }
            else
            {
                IsPinned.IsConfirmed = true;
                _context.HamkariRequests.Update(IsPinned);
                _context.SaveChanges();
                return 1;
            }
        }
        [HttpGet("CreateRezome")]
        public async Task<ActionResult<Rezome>> CreateRezome(string UserId, IFormFile Image, Rezome rezome, bool IsEdite)
        {
            var Userselected = _context.Jobsearchers.Include(p => p.User).Where(p => p.UserID == UserId).FirstOrDefault();

            var uploads = Path.Combine(_env.WebRootPath, "EmployerPic");
            if (Directory.Exists(uploads))
            {
                //if (ModelState.IsValid)
                //{

                string ImagePath = "";
                if (Image == null && !IsEdite)
                {
                    ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\RezomePic", "nophoto.jpg");
                    rezome.Image = "RezomePic/nophoto.jpg";
                }
                else
                {
                    if (IsEdite && Image != null)
                    {
                        string DeleteFileDirectory = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", rezome.Image);
                        try
                        {
                            if (System.IO.File.Exists(DeleteFileDirectory))
                            {
                                System.IO.File.Delete(DeleteFileDirectory);

                            }
                        }
                        catch (Exception)
                        {

                        }
                        ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\RezomePic", rezome.PhoneNumber + Image.FileName);
                        rezome.Image = "RezomePic/" + rezome.PhoneNumber + Image.FileName;
                        using (var fileStream = new FileStream(ImagePath, FileMode.Create))
                        {
                            await Image.CopyToAsync(fileStream);

                        }

                        rezome.IsConfirm = true;
                        _context.Update(rezome);

                    }
                    if (IsEdite && Image == null)
                    {
                        rezome.IsConfirm = true;

                        _context.Update(rezome);

                    }
                    if (!IsEdite && Image != null)
                    {
                        string RandomCode = Userselected.UinqCode;
                        ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\RezomePic", rezome.PhoneNumber + Image.FileName);
                        rezome.Image = "RezomePic/" + rezome.PhoneNumber + Image.FileName;
                        using (var fileStream = new FileStream(ImagePath, FileMode.Create))
                        {
                            await Image.CopyToAsync(fileStream);

                        }
                        rezome.UinqCode = RandomCode;
                        string Url = Appsetting.BaseUrlSite + "Rezomes/ShowRezomeForUser";
                        await CreateQrPic(Url, RandomCode);
                        rezome.IsConfirm = true;

                        _context.Add(rezome);
                    }


                }

                await _context.SaveChangesAsync();
                return rezome;
                //}
            }
            else
            {
                return null;
            }
        }
        public async Task CreateQrPic(string RezomeUrl, string QrFileName)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(RezomeUrl, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            string ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\RezomePic\\QrImage", QrFileName + ".Jpeg");

            System.IO.FileStream fs = System.IO.File.Open(ImagePath, FileMode.Create);
            qrCodeImage.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
            qrCodeImage.Dispose();
            fs.Close();
        }

        #endregion

        #region Employer

        [HttpGet("RegisterEmployer")]
        public async Task<ActionResult<Employer>> RegisterEmployer(IFormFile Image, Employer employer)
        {
            var uploads = Path.Combine(_env.WebRootPath, "EmployerPic");
            if (!Directory.Exists(uploads))
            {
                try
                {
                    Directory.CreateDirectory(uploads);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = employer.PhoneNumber, Email = employer.PhoneNumber };
                var result = await _userManager.CreateAsync(user, employer.Password);
                if (result.Succeeded)
                {

                    string ImagePath = "";
                    if (Image == null)
                    {
                        ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\EmployerPic", "nophoto.jpg");
                        employer.Image = "EmployerPic/nophoto.jpg";
                    }
                    else
                    {
                        ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\EmployerPic", employer.PhoneNumber + Image.FileName);
                        employer.Image = "EmployerPic/" + employer.PhoneNumber + Image.FileName;
                        using (var fileStream = new FileStream(ImagePath, FileMode.Create))
                        {
                            await Image.CopyToAsync(fileStream);
                        }
                    }


                    employer.UserID = user.Id;
                    _context.Employer.Add(employer);
                    bool adminRoleExists = await _roleManager.RoleExistsAsync("Employer");
                    if (!adminRoleExists)
                    {
                        await _roleManager.CreateAsync(new IdentityRole("Employer"));
                    }

                    // assign an existing role to the newly created user
                    await _userManager.AddToRoleAsync(user, "Employer");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "این شماره تلفن قبلا ثبت شده است");
                    return null;
                }
                await _context.SaveChangesAsync();

                return employer;
            }
            else
            {
                return null;
            }
        }
        #endregion


        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
