﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Areas.WebService.Models
{
    public class ApiFirstPage
    {
        public List<ApiFirstPageTitles> Titles { get; set; }
    }
}
