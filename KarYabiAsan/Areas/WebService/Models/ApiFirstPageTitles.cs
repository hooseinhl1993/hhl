﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static KarYabiAsan.Areas.WebService.Models.AppSetting;

namespace KarYabiAsan.Areas.WebService.Models
{
    public class ApiFirstPageTitles
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public ModeFirstPageTitle ModeTitle { get; set; }
    }
}
