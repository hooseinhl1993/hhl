﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Areas.WebService.Models
{
    public class AppSetting
    {
        public enum ModeFirstPageTitle
        {
            Job=0,
            Rezome=1,
            Both=2
        }
        public static int PageCount { get; set; } = 25;
    }
}
