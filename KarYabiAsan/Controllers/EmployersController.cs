﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KarYabiAsan.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using KarYabiAsan.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using GlobalClass;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;

namespace KarYabiAsan.Controllers
{
    
    public class EmployersController : Controller
    {
        private readonly KarYabiDBContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private IHostingEnvironment _env;
        private readonly RoleManager<IdentityRole> _roleManager;

        public EmployersController(KarYabiDBContext context, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, IHostingEnvironment env,
             RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
            _signInManager = signInManager;
            _userManager = userManager;
            _context = context;
            _env = env;
        }

        // GET: Employers
     
        [AllowAnonymous]
        public async Task<IActionResult> LogIn()
        {
            return View();
        }
        [HttpPost]
        [RequiresAuthentication]
        public async Task<IActionResult> LogIn(string PhoneNumber, string Password)
        {
            var result = await _signInManager.PasswordSignInAsync(PhoneNumber, Password, true, lockoutOnFailure: false);
            if (result.Succeeded)
            {

                return RedirectToAction(nameof(Details), "Employers", new { Un = AESEncryption.EncryptData(PhoneNumber, AESEncryption.KeyTelma), Ps = AESEncryption.EncryptData(Password, AESEncryption.KeyTelma) });
            }
            else
            {
                ModelState.AddModelError("", "کارفرمایی با این مشخصات وجود ندارد");
                return View();
            }
        }
        // GET: Employers/Details/5
        [RequiresAuthentication]
        public async Task<IActionResult> Details(string Un = "", string Ps = "")
        {

            if (!User.IsInRole("Employer"))
            {
                await _signInManager.SignOutAsync();
                ModelState.AddModelError(string.Empty, "این بخش مربوط به کارجویان است");
                return RedirectToAction(nameof(LogIn));
            }
            var user = await _userManager.GetUserAsync(User);
            var employer = _context.Employer.Include(p => p.User).Where(p => p.User == user).FirstOrDefault();
            if (employer == null)
            {
                return RedirectToAction(nameof(LogIn));
            }
            if (!user.PhoneNumberConfirmed)
            {
                return RedirectToAction(nameof(SendValidateCode), new { PhoneNumber = employer.PhoneNumber, UserId = user.Id, Un = Un, Ps = Ps });
            }
            var joblist = _context.Job.Where(p => p.Employer == employer.ID).ToList().OrderByDescending(p => p.JobId);
            EployerJobViewModel eployerJobViewModel = new EployerJobViewModel()
            {
                Employer = employer,
                Jobs = joblist
            };
            return View(eployerJobViewModel);
        }
        [HttpGet]
        [RequiresAuthentication]
        public async Task<IActionResult> SendValidateCode(string PhoneNumber, string UserId, string Un = "", string ps = "")
        {
            var user = await _userManager.GetUserAsync(User);

            if (user.PhoneNumberConfirmed)
            {
                ModelState.AddModelError("Registered", "این شماره قبلا ثبت شده");
                return RedirectToAction(nameof(LogIn));
            }
            ViewBag.PhoneNumber = PhoneNumber;
            ViewBag.Un = Un;
            ViewBag.ps = ps;
            await SendVerifyCode(PhoneNumber, UserId);
            return View();
        }
        [RequiresAuthentication]
        public async Task<IActionResult> VeriFyUserCode(string PhoneNumber, string Code, string Un = "", string ps = "")
        {
            var user = await _userManager.GetUserAsync(User);
            var isvalid = _context.UsersVerifies.Where(p => p.PhoneNumber == PhoneNumber && p.Code == int.Parse(Code)).ToList().Count() > 0 ? true : false;

            if (isvalid)
            {
                user.PhoneNumberConfirmed = true;
                await _userManager.UpdateAsync(user);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { Un = Un, Ps = ps });
            }
            else
            {
                await SendVerifyCode(PhoneNumber, user.Id);
                ViewBag.PhoneNumber = PhoneNumber;
                ModelState.AddModelError("ExpireCode", "این کد منقضی شده است");
                return RedirectToAction(nameof(Details), new { Un = Un, Ps = ps });
            }

        }
        [RequiresAuthentication]
        public async Task<int> SendMessageAgain()
        {
            var user = await _userManager.GetUserAsync(User);
            var SendedMessages = _context.UsersVerifies.Where(p => p.dateTime > DateTime.Now.AddMinutes(-6) && p.PhoneNumber == user.Email).ToList();
            if (SendedMessages.Count < 5)
            {
                await SendVerifyCode(user.Email, user.Id);
                return 1;
            }
            else
            {
                return 0;
            }

        }
        [RequiresAuthentication]
        public async Task SendVerifyCode(string PhoneNumber, string UserId)
        {
            Random random = new Random();
            int value = random.Next(1001, 9895);
            var Verify = new UsersVerifyMessage()
            {
                PhoneNumber = PhoneNumber,
                Code = value,
                dateTime = DateTime.Now
            };
            _context.UsersVerifies.Add(Verify);
            await _context.SaveChangesAsync();
            await SMS.KasBarg.SendCodeVerify(UserId, PhoneNumber, value + "");
        }
        [RequiresAuthentication]
        // GET: Employers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Employers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequiresAuthentication]
        public async Task<IActionResult> Create(IFormFile Image, Employer employer)
        {
            var uploads = Path.Combine(_env.WebRootPath, "EmployerPic");
            if (!Directory.Exists(uploads))
            {
                try
                {
                    Directory.CreateDirectory(uploads);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = employer.PhoneNumber, Email = employer.PhoneNumber };
                var result = await _userManager.CreateAsync(user, employer.Password);
                if (result.Succeeded)
                {

                    string ImagePath = "";
                    if (Image == null)
                    {
                        ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\EmployerPic", "nophoto.jpg");
                        employer.Image = "EmployerPic/nophoto.jpg";
                    }
                    else
                    {
                        ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\EmployerPic", employer.PhoneNumber + Image.FileName);
                        employer.Image = "EmployerPic/" + employer.PhoneNumber + Image.FileName;
                        using (var fileStream = new FileStream(ImagePath, FileMode.Create))
                        {
                            await Image.CopyToAsync(fileStream);
                        }
                    }


                    employer.UserID = user.Id;
                    _context.Employer.Add(employer);
                    bool adminRoleExists = await _roleManager.RoleExistsAsync("Employer");
                    if (!adminRoleExists)
                    {
                        await _roleManager.CreateAsync(new IdentityRole("Employer"));
                    }

                    // assign an existing role to the newly created user
                    await _userManager.AddToRoleAsync(user, "Employer");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "این شماره تلفن قبلا ثبت شده است");
                    return View(employer);
                }
                await _context.SaveChangesAsync();
                await _signInManager.PasswordSignInAsync(employer.PhoneNumber, employer.Password, true, lockoutOnFailure: false);

                return RedirectToAction(nameof(Details), new { id = employer.ID });
            }
            return View(employer);

        }


        // GET: Jobs/Create
        [RequiresAuthentication]
        public async Task<IActionResult> CreateJob()
        {
            var user = await _userManager.GetUserAsync(User);
            var employer = _context.Employer.Include(p => p.User).Where(p => p.User == user).FirstOrDefault();
            ViewData["ComponyName"] = employer.Name;
            ViewData["EmployerId"] = employer.ID;
            Job_Category_ViewModel viewModel = new Job_Category_ViewModel();
            viewModel.JobCategory = await _context.JobCategories.ToListAsync();
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequiresAuthentication]
        public async Task<IActionResult> CreateJob(Job job)
        {

            job.ComponyImage = _context.Employer.SingleOrDefault(p => p.ID == job.Employer).Image;
            job.CreateDate = DateTime.Now;
            job.CreateDateShow = await Appsetting.ConvertMiladiToShamsi(DateTime.Now);
            _context.Add(job);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new { id = job.Employer });

        }
        [Authorize]
        public async Task<IActionResult> Exit()
        {

            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        // GET: Jobs/Create
        [RequiresAuthentication]
        public async Task<IActionResult> EditJob(int Id)
        {
            if (Id == 0)
            {
                return NotFound();
            }
            var Job = _context.Job.Where(p => p.JobId == Id).FirstOrDefault();
            if (Job == null)
            {
                return NotFound();
            }
            Job_Category_ViewModel viewModel = new Job_Category_ViewModel();
            viewModel.JobCategory = await _context.JobCategories.ToListAsync();
            viewModel.Job = Job;
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequiresAuthentication]
        public async Task<IActionResult> EditJob(Job job)
        {

            _context.Job.Update(job);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(DetailsJobEmployer), new { id = job.JobId });

        }



        // GET: Employers/Edit/5
        [RequiresAuthentication]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employer = await _context.Employer.FindAsync(id);
            if (employer == null)
            {
                return NotFound();
            }
            return View(employer);
        }
        [RequiresAuthentication]
        public async Task<IActionResult> DetailsJobEmployer(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var job = await _context.Job
                .FirstOrDefaultAsync(m => m.JobId == id);
            if (job == null)
            {
                return NotFound();
            }
            Job_Category_ViewModel viewModel = new Job_Category_ViewModel();
            viewModel.JobCategory = await _context.JobCategories.ToListAsync();
            viewModel.Job = job;
            viewModel.Employer = await _context.Employer.Where(p => p.ID == job.Employer).FirstOrDefaultAsync();
            var UniqsIds = _context.Jobsearchers.Where(p => _context.SendRezomes.Where(x => x.JobId == id).Select(x => x.UserId).Contains(p.UserID)).ToList().Select(p => p.UinqCode).ToList();
            ViewBag.SendRezome = _context.Rezomes.Where(p => UniqsIds.Contains(p.UinqCode)).ToList();

            return View(viewModel);
        }

        [AllowAnonymous]
        public async Task<int> SendHamkariRequest(string IdRezome)
        {
            //var user = await _userManager.GetUserAsync(User);
            //var employer = _context.Employer.Include(p => p.User).Where(p => p.User == user).FirstOrDefault();
            if (!User.Identity.IsAuthenticated)
            {
                return -1;
            }
            if (!User.IsInRole("Employer"))
            {
                return -2;
            }
            var user = await _userManager.GetUserAsync(User);
            var IsPinned = _context.HamkariRequests.Where(p => p.RezomeId == IdRezome && p.EmployerId == user.Id).FirstOrDefault();
            if (IsPinned != null)
            {
                _context.HamkariRequests.Remove(IsPinned);
                _context.SaveChanges();
                return 0;
            }
            else
            {
                _context.HamkariRequests.Add(new HamkariRequest() { RezomeId = IdRezome, EmployerId = user.Id, RequestDate = DateTime.Now, RequestDateShow = await Appsetting.ConvertMiladiToShamsi(DateTime.Now), IsConfirmed = false });
                _context.SaveChanges();
                return 1;
            }
        }

        // POST: Employers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequiresAuthentication]
        public async Task<IActionResult> Edit(int id, Employer employer)
        {
            if (id != employer.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    //if (!EmployerExists(employer.ID))
                    //{
                    //    return NotFound();
                    //}
                    //else
                    //{
                    //    throw;
                    //}
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employer);
        }

    }
}
public class RequiresAuthenticationAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        //You can put your check here. This particular
        //check is for default asp.net membership authentication

        if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
        {
            RedirectToLogin(filterContext);
        }
    }

    private void RedirectToLogin(ActionExecutingContext filterContext)
    {
        var redirectTarget = new RouteValueDictionary
    {
        {"action", "LogIn"},
        {"controller", "Employers"}
    };

        filterContext.Result = new RedirectToRouteResult(redirectTarget);
    }
}