﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using KarYabiAsan.Models;
using KarYabiAsan.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Identity;

namespace KarYabiAsan.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly KarYabiDBContext _context;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;

        public HomeController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, ILogger<HomeController> logger, KarYabiDBContext context)
        {
            _logger = logger;
            _context = context;
            _signInManager = signInManager;
            _userManager = userManager;

        }

        public async Task<IActionResult> Index()
        {
            FirstPageViewModel firestPageViewModel = new FirstPageViewModel();
            //دسته بندی کار ها برای جستجو
            firestPageViewModel.Categories = await _context.JobCategories.ToListAsync();
            //کارهای فوری امروز
            firestPageViewModel.ForiEmrozJobs = await _context.Job.Where(p => p.IsFori&&p.IsConfirm).OrderByDescending(p => p.JobId).Take(12).ToListAsync();
            //آخرین کار های امروز
            firestPageViewModel.LastJobs = await _context.Job.Where(p=>p.IsConfirm).OrderByDescending(p => p.JobId).Take(8).ToListAsync();
            //رزومه های فوری امروز
            firestPageViewModel.ForiRezome = await _context.Rezomes.Where(p => p.IsFori&&p.IsConfirm).OrderByDescending(p => p.Id).ToListAsync();
            //آخرین رزومه ها
            firestPageViewModel.LastRezomes = await _context.Rezomes.Where(p => p.IsConfirm).OrderByDescending(p => p.Id).Take(8).ToListAsync();
            ////آخرن کار های امروز
            //firestPageViewModel.Jobsearchers = await _context.Jobsearchers.OrderByDescending(p => p.Id).Take(16).ToListAsync();
            ViewBag.EmployerCount = _context.Employer.Count();
            ViewBag.JobResearcherCount = _context.Jobsearchers.Count();
            ViewBag.RezomesCount = _context.Rezomes.Count();
            ViewBag.JobCount = _context.Job.Count();
            ViewBag.JobCategoryList = await _context.JobCategories.ToListAsync();
            return View(firestPageViewModel);
        }


        /// <summary>
        /// برای ورود از سمت موبایل
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public async Task<IActionResult> EnterFormMobile(string UserId, string Password)
        {
            if (!string.IsNullOrEmpty(UserId) && !string.IsNullOrEmpty(Password))
            {
                var result = await _signInManager.PasswordSignInAsync(UserId, Password, true, lockoutOnFailure: false);

            }
            return RedirectToAction(nameof(Index));
        }




        public async Task<IActionResult> SearchJob(string JobTitle, int Ostan, int JobCategory, int page = 1)
        {
            List<Job> FindedJobs = new List<Job>();
            List<string> TitleList = new List<string>();
            if (!string.IsNullOrEmpty(JobTitle))
            {
                TitleList = JobTitle.Split(' ').ToList();
                TitleList.Add(JobTitle);

            }
            var R = GetExpression(TitleList, Ostan, JobCategory);
            if (R != null)
            {
                FindedJobs = _context.Job.Where(R).ToList();

            }
            else
            {
                FindedJobs = await _context.Job.Where(p=>p.IsConfirm).ToListAsync();
            }
            
            ViewBag.JobCategoryList = await _context.JobCategories.ToListAsync();
            int offset = (Appsetting.PageSize * page) - Appsetting.PageSize;
            ViewBag.ItemsCount = FindedJobs.Count();
            ViewBag.PageNumber = page;
            FindedJobs = FindedJobs.OrderByDescending(p => p.JobId).Skip(offset).Take(Appsetting.PageSize).ToList();
            var user = await _userManager.GetUserAsync(User);

            if (user != null)
            {
                var IsPinned = _context.BadgedJobs.Where(p => p.UserId == user.Id).Select(p => p.JobId).ToList();
                var IsSended = _context.SendRezomes.Where(p => p.UserId == user.Id).Select(p => p.JobId).ToList();

                FindedJobs.Where(p => IsPinned.Contains(p.JobId)).ToList().ForEach(p => p.IsPinned = true);       
                FindedJobs.Where(p => IsSended.Contains(p.JobId)).ToList().ForEach(p => p.IsSend = true);
            }
            return View(FindedJobs);
        }
        public Func<Job, bool> GetExpression(List<string> listTitle, int Ostan, int JobCategory)
        {
            Func<Job, bool> R = null;
             R+= X => X.IsConfirm;
            if (listTitle.Count > 0)
            {
                foreach (var item in listTitle)
                {
                    R += X => X.Title.Contains(item);
                }
            }
            if (Ostan != 0)
            {
                R += X => X.State == (States)Ostan;
            }
            if (JobCategory != 0)
            {
                R += X => X.CateGory == JobCategory;
            }
            return R;
        }

        public async Task<IActionResult> SearchRezome(string JobTitle, int Ostan, int JobCategory, int page = 1)
        {
            List<Rezome> FindedJobs = new List<Rezome>();
            List<string> TitleList = new List<string>();
            if (!string.IsNullOrEmpty(JobTitle))
            {
                TitleList = JobTitle.Split(' ').ToList();
                TitleList.Add(JobTitle);

            }
            var R = GetExpressionRezome(TitleList, Ostan, JobCategory);
            if (R != null)
            {
                FindedJobs = _context.Rezomes.Where(R).ToList();

            }
            else
            {
                FindedJobs = await _context.Rezomes.Where(p => p.IsConfirm).ToListAsync();
            }
            ViewBag.JobCategoryList = await _context.JobCategories.ToListAsync();
            int offset = (Appsetting.PageSize * page) - Appsetting.PageSize;
            ViewBag.ItemsCount = FindedJobs.Count();
            ViewBag.PageNumber = page;
            FindedJobs = FindedJobs.OrderByDescending(p => p.Id).Skip(offset).Take(Appsetting.PageSize).ToList();
            var user = await _userManager.GetUserAsync(User);
            if (user != null)
            {
                var IsSended = _context.HamkariRequests.Where(p => p.EmployerId == user.Id).Select(p => p.RezomeId).ToList();
                var IsSendConfimed = _context.HamkariRequests.Where(p => p.EmployerId == user.Id&&p.IsConfirmed).Select(p => p.RezomeId).ToList();

                FindedJobs.Where(p => IsSended.Contains(p.UinqCode)).ToList().ForEach(p => p.requesSended = true);
                FindedJobs.Where(p => IsSendConfimed.Contains(p.UinqCode)).ToList().ForEach(p => p.requesConfirmed = true);
            }

            return View(FindedJobs);
        }
        public Func<Rezome, bool> GetExpressionRezome(List<string> listTitle, int Ostan, int RezomeCategory)
        {
            Func<Rezome, bool> R = null;
            R += X => X.IsConfirm;
            if (listTitle.Count > 0)
            {
                foreach (var item in listTitle)
                {
                    R += X => X.OnvanShoghl.Contains(item);
                }
            }
            if (Ostan != 0)
            {
                R += X => X.State == (States)Ostan;
            }
            if (RezomeCategory != 0)
            {
                R += X => X.RezomeCategory == RezomeCategory;
            }
            return R;
        }


        public IActionResult LogIn()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
