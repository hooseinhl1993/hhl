﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KarYabiAsan.Models;
using KarYabiAsan.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace KarYabiAsan.Controllers
{
    public class JobsController : Controller
    {
        private readonly KarYabiDBContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public JobsController(KarYabiDBContext context, UserManager<IdentityUser> userManager)
        {

            _userManager = userManager;

            _context = context;
        }

        // GET: Jobs
        public async Task<IActionResult> Index()
        {
            return View(await _context.Job.ToListAsync());
        }

        // GET: Jobs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var job = await _context.Job
                .FirstOrDefaultAsync(m => m.JobId == id);
            if (job == null)
            {
                return NotFound();
            }
            var user = await _userManager.GetUserAsync(User);
            if (user != null)
            {
                var IsPinned = _context.BadgedJobs.Where(p => p.UserId == user.Id).Select(p => p.JobId).ToList();
                var IsSended = _context.SendRezomes.Where(p => p.UserId == user.Id).Select(p => p.JobId).ToList();
                if (IsPinned.Contains(job.JobId))
                {
                    job.IsPinned = true;
                }
                if (IsSended.Contains(job.JobId))
                {
                    job.IsSend = true;
                }
            }

            Job_Category_ViewModel viewModel = new Job_Category_ViewModel();
            viewModel.JobCategory = await _context.JobCategories.ToListAsync();
            viewModel.Job = job;
            viewModel.Employer = await _context.Employer.Where(p => p.ID == job.Employer).FirstOrDefaultAsync();
            return View(viewModel);
        }

        public async Task<IActionResult> SendRezome(int JobID)
        {
            var UserJson = HttpContext.Session.GetString("User");
            jobsearcher User = new jobsearcher();
            if (!string.IsNullOrEmpty(UserJson))
            {
                User = Newtonsoft.Json.JsonConvert.DeserializeObject<jobsearcher>(UserJson);

            }
            var JobSearcheCode = _context.Rezomes.Where(p=>p.Id==User.Id).SingleOrDefault();
            if (JobSearcheCode==null)
            {
                ModelState.AddModelError("", "کارجویی با این مشخصات وجود ندارد");
                return RedirectToAction("Details", new { id = JobID });
            }
            var Rezome = _context.Rezomes.Where(p => p.JobSearcheCode == JobSearcheCode.JobSearcheCode).SingleOrDefault();
            if (Rezome==null)
            {
                ModelState.AddModelError("", "رزومه ای برای شما ثبت نشده");
                return RedirectToAction("Details", new { id = JobID });
            }
            var Rezome_Job = new Job_Rezome()
            {
                Job = JobID,
                Rezome = Rezome.UinqCode
            };
            _context.Job_Rezomes.Add(Rezome_Job);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details",new {id=JobID });
        }
       

        // GET: Jobs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var job = await _context.Job.FindAsync(id);
            if (job == null)
            {
                return NotFound();
            }
            return View(job);
        }

        // POST: Jobs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("JobId,Title,CateGory,State,City,Hamkari,MinimumWorkExperience,Salery,Decription,MyProperty,Gender,Educate,PayanKhedmat")] Job job)
        {
            if (id != job.JobId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(job);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JobExists(job.JobId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(job);
        }

        // GET: Jobs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var job = await _context.Job
                .FirstOrDefaultAsync(m => m.JobId == id);
            if (job == null)
            {
                return NotFound();
            }

            return View(job);
        }

        // POST: Jobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var job = await _context.Job.FindAsync(id);
            _context.Job.Remove(job);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool JobExists(int id)
        {
            return _context.Job.Any(e => e.JobId == id);
        }
    }
}
