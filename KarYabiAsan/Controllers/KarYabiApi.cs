﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Controllers
{
    [Route("api/KarYabi")]
    [ApiController]
    public class KarYabiApi : ControllerBase
    {
        /// <summary>
        /// ورژن بازار
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("IsBazar")]
        public object IsBazar()
        {
            return Ok(1);
        }
    }
}
