﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KarYabiAsan.Models;
using KarYabiAsan.Models.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using QRCoder;
using System.Drawing;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace KarYabiAsan.Controllers
{
    public class RezomesController : Controller
    {
        private readonly KarYabiDBContext _context;
        private IHostingEnvironment _env;
        private readonly UserManager<IdentityUser> _userManager;
        public RezomesController(KarYabiDBContext context, IHostingEnvironment env, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _env = env;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> ShowRezome(string Uq)
        {
            //var UserSelected = await _context.Jobsearchers.Where(p => p.UinqCode == Uq).SingleOrDefaultAsync();
            ViewBag.ShowHamkari = true;
            var rezom = await _context.Rezomes.Where(p => p.JobSearcheCode == Uq).SingleOrDefaultAsync();
            JobSearcherRezomeViewModel jobSearcherRezomeViewModel = new JobSearcherRezomeViewModel()
            {
                //jobsearcher = UserSelected,
                Rezome = rezom
            };
            return View( jobSearcherRezomeViewModel);
        }


        public async Task<IActionResult> ShowRezomeForUser()
        {
            var user = await _userManager.GetUserAsync(User);
            var UserSelected = await _context.Jobsearchers.Where(p => p.UserID == user.Id).SingleOrDefaultAsync();
            ViewBag.ShowHamkari = false;
            var rezom = await _context.Rezomes.Where(p => p.JobSearcheCode == UserSelected.UinqCode).SingleOrDefaultAsync();
            JobSearcherRezomeViewModel jobSearcherRezomeViewModel = new JobSearcherRezomeViewModel()
            {
                jobsearcher = UserSelected,
                Rezome = rezom
            };
            return View("ShowRezome", jobSearcherRezomeViewModel);
        }





        [Authorize(Roles = "JobSearcher")]
        public async Task<IActionResult> CreateRezome()
        {
            var user = await _userManager.GetUserAsync(User);
            var Userselected = _context.Jobsearchers.Include(p => p.User).Where(p => p.User == user).FirstOrDefault();

            JobSearcherRezomeViewModel jobSearcherRezomeViewModel = new JobSearcherRezomeViewModel();
            jobSearcherRezomeViewModel.Rezome = await _context.Rezomes.Where(p => p.JobSearcheCode == Userselected.UinqCode).SingleOrDefaultAsync();
            if (jobSearcherRezomeViewModel.Rezome != null)
            {
                ViewBag.IsEdite = true;
            }
            jobSearcherRezomeViewModel.jobsearcher = await _context.Jobsearchers.Where(p => p.UserID == Userselected.UserID).SingleOrDefaultAsync();
            jobSearcherRezomeViewModel.jobCategories = await _context.JobCategories.ToListAsync();
            return View(jobSearcherRezomeViewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "JobSearcher")]
        public async Task<IActionResult> CreateRezome(IFormFile Image, Rezome rezome, bool IsEdite)
        {
            var user = await _userManager.GetUserAsync(User);
            var Userselected = _context.Jobsearchers.Include(p => p.User).Where(p => p.User == user).FirstOrDefault();

            var uploads = Path.Combine(_env.WebRootPath, "EmployerPic");
            if (Directory.Exists(uploads))
            {
                //if (ModelState.IsValid)
                //{

                string ImagePath = "";
                if (Image == null && !IsEdite)
                {
                    ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\RezomePic", "nophoto.jpg");
                    rezome.Image = "RezomePic/nophoto.jpg";
                }
                else
                {
                    if (IsEdite && Image != null)
                    {
                        string DeleteFileDirectory = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", rezome.Image);
                        try
                        {
                            if (System.IO.File.Exists(DeleteFileDirectory))
                            {
                                System.IO.File.Delete(DeleteFileDirectory);

                            }
                        }
                        catch (Exception)
                        {

                        }
                        ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\RezomePic", rezome.PhoneNumber + Image.FileName);
                        rezome.Image = "RezomePic/" + rezome.PhoneNumber + Image.FileName;
                        using (var fileStream = new FileStream(ImagePath, FileMode.Create))
                        {
                            await Image.CopyToAsync(fileStream);

                        }

                        rezome.IsConfirm = true;
                        _context.Update(rezome);

                    }
                    if (IsEdite && Image == null)
                    {
                        rezome.IsConfirm = true;

                        _context.Update(rezome);

                    }
                    if (!IsEdite && Image != null)
                    {
                        string RandomCode = Userselected.UinqCode;
                        ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\RezomePic", rezome.PhoneNumber + Image.FileName);
                        rezome.Image = "RezomePic/" + rezome.PhoneNumber + Image.FileName;
                        using (var fileStream = new FileStream(ImagePath, FileMode.Create))
                        {
                            await Image.CopyToAsync(fileStream);

                        }
                        rezome.UinqCode = RandomCode;
                       string Url= Appsetting.BaseUrlSite+ "Rezomes/ShowRezomeForUser";
                       await CreateQrPic(Url, RandomCode);
                        rezome.IsConfirm = true;

                        _context.Add(rezome);
                    }


                }

                await _context.SaveChangesAsync();
                return RedirectToAction("ShowRezomeForUser");
                //}
            }
            return View();
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public async Task CreateQrCode()
        {
            var user = _context.Jobsearchers.ToList();
            foreach (var item in user)
            {
                string Url = Appsetting.BaseUrlSite + "Rezomes/ShowRezome";
                await CreateQrPic(Url, item.UinqCode);

            }
        }

        public async Task CreateQrPic(string RezomeUrl,string QrFileName)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(RezomeUrl, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            string ImagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\RezomePic\\QrImage", QrFileName+".Jpeg");

            System.IO.FileStream fs = System.IO.File.Open(ImagePath , FileMode.Create);
            qrCodeImage.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
            qrCodeImage.Dispose();
            fs.Close();
        }
        // This method is for converting bitmap into a byte array
        private static byte[] BitmapToBytes(Bitmap img)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }




    }
}
