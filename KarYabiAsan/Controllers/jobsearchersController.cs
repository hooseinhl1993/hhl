﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KarYabiAsan.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using SelectPdf;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using KarYabiAsan.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace KarYabiAsan.Controllers
{
    [Authorize(Roles = "JobSearcher")]
    public class jobsearchersController : Controller
    {
        private readonly KarYabiDBContext _context;
        private IHostingEnvironment _env;
        private readonly Microsoft.AspNetCore.Mvc.ViewEngines.ICompositeViewEngine _viewEngine;
        private readonly SignInManager<IdentityUser> _SignManager;
        private readonly UserManager<IdentityUser> _userManager;

        public jobsearchersController(KarYabiDBContext context, IHostingEnvironment env, ICompositeViewEngine viewEngine, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> SignManager)
        {
            _context = context;
            _env = env;
            _viewEngine = viewEngine;
            _userManager = userManager;
            _SignManager = SignManager;
        }

        public async Task<IActionResult> LogIn()
        {
            //if (!User.IsInRole("JobSearcher"))
            //{
            //    ModelState.AddModelError("", "این بخش مربوط به کارجویان است ");
            //    await _SignManager.SignOutAsync();
            //    return View();
            //}
            return RedirectToAction(nameof(Details));
        }

        public async Task<IActionResult> Details(string Un = "", string Ps = "")
        {
            var user = await _userManager.GetUserAsync(User);
            if (!User.IsInRole("JobSearcher"))
            {
                await _SignManager.SignOutAsync();
                ModelState.AddModelError(string.Empty, "این بخش مربوط به کارجویان است");
                return RedirectToAction(nameof(LogIn));
            }
            var Userselected = _context.Jobsearchers.Include(p => p.User).Where(p => p.User == user).FirstOrDefault();
            if (Userselected == null)
            {
                return RedirectToAction(nameof(LogIn));
            }

            var jobsearcher = await _context.Jobsearchers
                .FirstOrDefaultAsync(m => m.UserID == user.Id);

            if (!user.PhoneNumberConfirmed)
            {
                return RedirectToAction(nameof(SendValidateCode), new { PhoneNumber = Userselected.Phone, UserId = user.Id, Un = Un, Ps = Ps });
            }
            ViewBag.LastForiJob = _context.Job.OrderBy(p => p.JobId).Take(4).ToList();

            ViewBag.SendRezome = _context.Job.Where(p => _context.SendRezomes.Where(x => x.UserId == user.Id).Select(x => x.JobId).Contains(p.JobId)).ToList();
            ViewBag.SendHamkari = _context.HamkariRequests.Where(p => p.RezomeId == Userselected.UinqCode).ToList();

            ViewBag.PinJob = _context.Job.Where(p => _context.BadgedJobs.Where(x => x.UserId == user.Id).Select(x => x.JobId).Contains(p.JobId)).ToList();
            return View(jobsearcher);
        }



        [HttpGet]
        public async Task<IActionResult> SendValidateCode(string PhoneNumber, string UserId, string Un = "", string ps = "")
        {
            var user = await _userManager.GetUserAsync(User);

            if (user.PhoneNumberConfirmed)
            {
                ModelState.AddModelError("Registered", "این شماره قبلا ثبت شده");
                return RedirectToAction(nameof(LogIn));
            }
            ViewBag.PhoneNumber = PhoneNumber;
            ViewBag.Un = Un;
            ViewBag.ps = ps;
            await SendVerifyCode(PhoneNumber, UserId);
            return View();
        }

        public async Task<IActionResult> VeriFyUserCode(string PhoneNumber, string Code, string Un = "", string ps = "")
        {
            var user = await _userManager.GetUserAsync(User);
            var isvalid = _context.UsersVerifies.Where(p => p.dateTime.Minute - DateTime.Now.Minute <= 2).ToList().Count() > 0 ? true : false;

            if (isvalid)
            {
                user.PhoneNumberConfirmed = true;
                await _userManager.UpdateAsync(user);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { Un = Un, Ps = ps });
            }
            else
            {
                await SendVerifyCode(PhoneNumber, user.Id);
                ViewBag.PhoneNumber = PhoneNumber;
                ModelState.AddModelError("ExpireCode", "این کد منقضی شده است");
                return RedirectToAction(nameof(Details), new { Un = Un, Ps = ps });
            }

        }
        public async Task<int> SendMessageAgain()
        {
            var user = await _userManager.GetUserAsync(User);
            var SendedMessages = _context.UsersVerifies.Where(p => p.dateTime > DateTime.Now.AddMinutes(-6) && p.PhoneNumber == user.Email).ToList();
            if (SendedMessages.Count < 5)
            {
                await SendVerifyCode(user.Email, user.Id);
                return 1;
            }
            else
            {
                return 0;
            }



        }
        public async Task SendVerifyCode(string PhoneNumber, string UserId)
        {
            Random random = new Random();
            int value = random.Next(1001, 9895);
            var Verify = new UsersVerifyMessage()
            {
                PhoneNumber = PhoneNumber,
                Code = value,
                dateTime = DateTime.Now
            };
            _context.UsersVerifies.Add(Verify);
            await _context.SaveChangesAsync();
            await SMS.KasBarg.SendCodeVerify(UserId, PhoneNumber, value + "");
        }

        // GET: jobsearchers/Details/5


        // GET: jobsearchers/Edit/5
        public async Task<IActionResult> Edit(jobsearcher jobsearcher)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("ExpireCode", "اطلاعات را به درستی وارد کنید");
                return RedirectToAction(nameof(Details));
            }

            _context.Jobsearchers.Update(jobsearcher);
            _context.SaveChanges();
            return RedirectToAction(nameof(Details));

        }



        public async Task<IActionResult> CreateRezomePDF()
        {

            var user = await _userManager.GetUserAsync(User);
            var Userselected = _context.Jobsearchers.Include(p => p.User).Where(p => p.User == user).FirstOrDefault();
            if (Userselected == null)
            {
                return RedirectToAction(nameof(LogIn));
            }

            var jobsearcher = await _context.Jobsearchers
                .FirstOrDefaultAsync(m => m.UserID == user.Id);

            if (!user.PhoneNumberConfirmed)
            {
                return RedirectToAction(nameof(SendValidateCode), new { PhoneNumber = Userselected.Phone, UserId = user.Id, Un = "", Ps = "" });
            }


            var Rezome = await RenderPartialViewToString("_PartialRezome", Userselected.UserID);
            SelectPdf.HtmlToPdf htmlToPdf = new SelectPdf.HtmlToPdf();
            PdfDocument pdfDocument = htmlToPdf.ConvertHtmlString(Rezome);
            byte[] pdf = pdfDocument.Save();
            pdfDocument.Close();
            return File(
                pdf,
                "application/pdf",
                "rezome.pdf"
                );
        }

        private async Task<string> RenderPartialViewToString(string viewName, string UserId)
        {
            var user = await _userManager.GetUserAsync(User);
            var Userselected = _context.Jobsearchers.Include(p => p.User).Where(p => p.User == user).FirstOrDefault();

            var rezom = await _context.Rezomes.Where(p => p.JobSearcheCode == "").SingleOrDefaultAsync();

            JobSearcherRezomeViewModel jobSearcherRezomeViewModel = new JobSearcherRezomeViewModel()
            {
                jobsearcher = Userselected,
                Rezome = rezom
            };
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.ActionDescriptor.ActionName;

            ViewData.Model = jobSearcherRezomeViewModel;

            using (var writer = new StringWriter())
            {
                ViewEngineResult viewResult =
                    _viewEngine.FindView(ControllerContext, viewName, false);

                ViewContext viewContext = new ViewContext(
                    ControllerContext,
                    viewResult.View,
                    ViewData,
                    TempData,
                    writer,
                    new Microsoft.AspNetCore.Mvc.ViewFeatures.HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);

                return writer.GetStringBuilder().ToString();
            }
        }

        [Authorize]
        public async Task<IActionResult> Exit()
        {
            //var Rezomes = _context.Rezomes.ToList();
            //foreach (var item in Rezomes)
            //{
            //    var user = new IdentityUser { UserName = item.PhoneNumber, Email = item.PhoneNumber };
            //    var result = await _userManager.CreateAsync(user, "123456");
            //    if (result.Succeeded)
            //    {
            //        _context.Jobsearchers.Add(new Models.jobsearcher() { UserID = user.Id, Email = "", Token = user.Id, Phone = item.PhoneNumber, Name = item.NameAndUserName, UinqCode = item.UinqCode });
            //        _context.SaveChanges();
            //    }
            //}

            await _SignManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }


        [AllowAnonymous]
        public async Task<int> PinJob(int JobId)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return -1;
            }
            var user = await _userManager.GetUserAsync(User);
            var IsPinned = _context.BadgedJobs.Where(p => p.JobId == JobId && p.UserId == user.Id).FirstOrDefault();
            if (IsPinned != null)
            {
                _context.BadgedJobs.Remove(IsPinned);
                _context.SaveChanges();
                return 0;
            }
            else
            {
                _context.BadgedJobs.Add(new BadgedJobs() { JobId = JobId, UserId = user.Id });
                _context.SaveChanges();
                return 1;
            }


        }

        [AllowAnonymous]
        public async Task<int> SendRezome(int JobId)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return -1;
            }
            var user = await _userManager.GetUserAsync(User);
            var IsPinned = _context.SendRezomes.Where(p => p.JobId == JobId && p.UserId == user.Id).FirstOrDefault();
            if (IsPinned != null)
            {
                _context.SendRezomes.Remove(IsPinned);
                _context.SaveChanges();
                return 0;
            }
            else
            {
                _context.SendRezomes.Add(new SendRezome() { JobId = JobId, UserId = user.Id });
                _context.SaveChanges();
                return 1;
            }


        }
        public async Task<int> ConfirmHamkariRequest(int IdRquest)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return -1;
            }
            var user = await _userManager.GetUserAsync(User);
            var IsPinned = _context.HamkariRequests.Where(p => p.Id == IdRquest).FirstOrDefault();
            if (IsPinned.IsConfirmed)
            {
                IsPinned.IsConfirmed = false;
                _context.HamkariRequests.Update(IsPinned);
                _context.SaveChanges();
                return 0;
            }
            else
            {
                IsPinned.IsConfirmed = true;
                _context.HamkariRequests.Update(IsPinned);
                _context.SaveChanges();
                return 1;
            }


        }



        //// GET: jobsearchers
        //public async Task<IActionResult> Index()
        //{
        //    //foreach (var item in _context.tbl_Jobs.OrderByDescending(p => p.ID).ToList())
        //    //{
        //    //    if (_context.Employer.Where(p => p.Id == item.IdUser).ToList().Count == 0)
        //    //    {
        //    //        Employer employer = new Employer();
        //    //        employer.Id = item.IdUser;
        //    //        employer.Image = "EmployerPic/nophoto.jpg";
        //    //        employer.ModeEmployer = ModeEmployer.Person;
        //    //        employer.Name = item.Onvan_Sherkar_user;
        //    //        employer.Password = item.Phone_Hamrah_User;
        //    //        employer.PhoneNumber = item.Phone_Hamrah_User;
        //    //        employer.Description = "ثبت نشده";
        //    //        employer.EmailAddress = item.Email_User;
        //    //        _context.Employer.Add(employer);
        //    //    }

        //    //    var Job = new Job();
        //    //    Job.Address = item.Address_User;
        //    //    Job.CateGory = 37;
        //    //    Job.CompanyName = item.Onvan_Sherkar_user;
        //    //    Job.ComponyImage = "EmployerPic/nophoto.jpg";
        //    //    Job.CreateDate = item.NowDate;
        //    //    Job.CreateDateShow = await Appsetting.ConvertMiladiToShamsi(item.NowDate);
        //    //    Job.Decription = item.Info_User;
        //    //    Job.Educate = EnumHelper<ModeEducate>.GetValueFromName(item.Tahsil_User);
        //    //    Job.Employer = item.IdUser;
        //    //    Job.Gender = EnumHelper<ModeGender>.GetValueFromName(item.Male_User);
        //    //    Job.Hamkari = ModeHamkari.FullTime;
        //    //    Job.IsEnable = true;
        //    //    Job.IsFori = false;
        //    //    Job.MinimumWorkExperience = 0;
        //    //    Job.PayanKhedmat = ModePayanKhedmat.NoMatter;
        //    //    Job.Salery = 0;
        //    //    Job.Skills = item.Maharatha_User;
        //    //    Job.State = EnumHelper<States>.GetValueFromName(item.City_User);
        //    //    Job.Title = item.Onvan_Job_User;
        //    //    _context.Job.Add(Job);
        //    //    _context.Remove(item);
        //    //    _context.SaveChanges();
        //    //}


        //    foreach (var item in _context.Tbl_Rezomeh.ToList())
        //    {
        //        jobsearcher jobsearcher = new jobsearcher();
        //        string RandomCode = "KarYAbi-" + RandomString(8);
        //        while (_context.Jobsearchers.Where(p => p.UinqCode.Contains(RandomCode)).ToList().Count > 0)
        //        {
        //            RandomCode = "KarYAbi-" + RandomString(8);
        //        }
        //        jobsearcher.UinqCode = RandomCode;
        //        jobsearcher.Id = item.IdUser;
        //        while (_context.Jobsearchers.Where(p => p.Id == jobsearcher.Id).ToList().Count > 0)
        //        {
        //            jobsearcher.Id++;
        //        }
        //        jobsearcher.Name = item.NameFamily;
        //        jobsearcher.Email = item.Email;
        //        jobsearcher.Password = "123456789";
        //        _context.Add(jobsearcher);
        //        var Rezom = new Rezome();
        //        Rezom.AboutMe = item.Info;
        //        var Educational = new Educationalbackground();

        //        Educational.CollageName = "ثبت نشده";
        //        Educational.Description = "ثبت نشده";
        //        Educational.Educate = EnumHelper<ModeEducate>.GetValueFromName(item.Tahsilat.Split("&&")[0]);
        //        Educational.EducationFeild = item.Tahsilat.Split("&&")[0];
        //        Educational.EndDate = 1399;
        //        Educational.StartDate = 1399;
        //        Educational.ID = 1;
        //        Educational.StillIdCollage = false;

        //        Rezom._Educationalbackgrounds = JsonConvert.SerializeObject(Educational);
        //        Rezom.Gender = ModeGender.Nomatter;
        //        Rezom.IsEdited = false;
        //        Rezom.IsFori = false;
        //        Rezom.JobSearcheCode = jobsearcher.UinqCode;
        //        var lan = new Lanquege
        //        {
        //            LanName = ModeLanquege.Englisi,
        //            Level = SkillsLevel.Mobtadi
        //        };
        //        Rezom._Lanquege = JsonConvert.SerializeObject(lan);
        //        Rezom.Location = item.City;
        //        Rezom.State = EnumHelper<States>.GetValueFromName(item.City);
        //        Rezom.StateEshteghal = ModeStateEshteghal.BeDonbalKarBehtar;
        //        Rezom.Taahol = ModeTaahol.NoMatter;
        //        string RandomCode2 = "KarYAbi-" + RandomString(8);
        //        while (_context.Rezomes.Where(p => p.UinqCode.Contains(RandomCode)).ToList().Count > 0)
        //        {
        //            RandomCode2 = "KarYAbi-" + RandomString(8);
        //        }
        //        Rezom.UinqCode = RandomCode2;
        //        var workex = new WorkExperience
        //        {
        //            StillInJob = false,
        //            StartYear = 1399,
        //            StartMonth = ModeMonth.Farvardin,
        //            EndYear = 1399,
        //            EndMonth = ModeMonth.Farvardin,
        //            ID = 0,
        //            CompanyName = item.Savabegh.Split("&&")[1],
        //            Description = item.Savabegh.Split("&&")[3],
        //            JobTitle = item.Savabegh.Split("&&")[2]
        //        };
        //        Rezom._WorkExperiences = JsonConvert.SerializeObject(workex);
        //        Rezom.NameAndUserName = item.NameFamily;
        //        Rezom.OnvanShoghl = item.Job;
        //        Rezom.PayanKhedmat = ModePayanKhedmat.NoMatter;
        //        Rezom.PhoneNumber = item.Phone;
        //        Rezom.RezomeCategory = 37;
        //        Rezom.Skills = item.Maharat.Replace("@@", ",");
        //        Rezom.YearOfBrithday = 1399;
        //        _context.Rezomes.Add(Rezom);
        //        _context.Tbl_Rezomeh.Remove(item);
        //        _context.SaveChanges();
        //    }
        //    return View(await _context.Jobsearchers.ToListAsync());
        //}

        //public static class EnumHelper<T>
        //{
        //    public static T GetValueFromName(string name)
        //    {
        //        var type = typeof(T);
        //        if (!type.IsEnum) throw new InvalidOperationException();

        //        foreach (var field in type.GetFields())
        //        {
        //            var attribute = Attribute.GetCustomAttribute(field,
        //                typeof(DisplayAttribute)) as DisplayAttribute;
        //            if (attribute != null)
        //            {
        //                if (attribute.Name == name)
        //                {
        //                    return (T)field.GetValue(null);
        //                }
        //            }
        //            else
        //            {
        //                if (field.Name == name)
        //                    return (T)field.GetValue(null);
        //            }
        //        }

        //        throw new ArgumentOutOfRangeException("name");
        //    }
        //}

    }
}
