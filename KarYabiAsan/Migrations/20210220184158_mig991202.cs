﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KarYabiAsan.Migrations
{
    public partial class mig991202 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Rezomes",
                nullable: false,
                defaultValue: "");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.DropColumn(
                name: "Email",
                table: "Rezomes");

           
        }
    }
}
