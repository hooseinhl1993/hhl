﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KarYabiAsan.Migrations
{
    public partial class mig991211 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<bool>(
                name: "IsConfirm",
                table: "Rezomes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsConfirm",
                table: "Job",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsConfirm",
                table: "Rezomes");

            migrationBuilder.DropColumn(
                name: "IsConfirm",
                table: "Job");

    
        }
    }
}
