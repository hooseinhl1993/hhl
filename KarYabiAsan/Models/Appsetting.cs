﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class Appsetting
    {
        public static int PageSize { get; set; } = 15;
        public static string BaseUrlApi { get; set; } = $"http://localhost:3598/api/Job/";
        public static string BaseUrlSite { get; set; } = $"http://localhost:49968/";
        public static string GetCreateDay(DateTime dateTime)
        {
            int day= (int)(DateTime.Now - dateTime).TotalDays;
            if (day==0)
            {
                return "امروز";

            }
            else
            {
                return day + " روز پیش";
            }
        }
        public static async Task<string> ConvertMiladiToShamsi(DateTime date)
        {
            PersianCalendar pc = new PersianCalendar();
            string PersianDate = string.Format("{0}/{1}/{2}", pc.GetYear(date), pc.GetMonth(date), pc.GetDayOfMonth(date));
            return PersianDate;
        }
    }
}
