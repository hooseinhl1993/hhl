﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class BadgedJobs
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public int JobId { get; set; }
    }
}
