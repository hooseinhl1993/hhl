﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class Educationalbackground
    {
        public int ID { get; set; }
        [Display(Name ="رشته تحصیلی")]
        public string EducationFeild { get; set; }
        public string CollageName { get; set; }
        public ModeEducate Educate { get; set; }
        public int StartDate { get; set; }
        public int EndDate { get; set; }
        public bool StillIdCollage { get; set; }
        public string Description { get; set; }
    }
}
