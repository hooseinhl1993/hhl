﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KarYabiAsan.Models
{
    public partial class Employer
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public IdentityUser User { get; set; }

        [Display(Name="نوع کارفرما")]
        public ModeEmployer ModeEmployer { get; set; }
        [Display(Name = "نام کارفرما")]
        [Required(ErrorMessage = "نام کارفرما را وارد کنید")]
        public string Name { get; set; }
        [Display(Name = "شماره تماس")]
        [Required(ErrorMessage = "شماره تماس را وارد کنید")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Display(Name = "آدرس ایمیل")]
        [Required(ErrorMessage = "آدرس ایمیل را وارد کنید")]
        [DataType(DataType.EmailAddress,ErrorMessage ="مقدار ورودی باید ایمیل باشد")]
        public string EmailAddress { get; set; }
        [Display(Name ="رمز عبور")]
        [Required(ErrorMessage = "رمز عبور را وارد کنید")]
        [StringLength(255, ErrorMessage = "رمز عبور باید بیشتر از 6 کاراکتر باشد", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name = " تکرار رمز عبور ")]
        [Required(ErrorMessage = "تکرار رمز عبور راوارد کنید")]
        [StringLength(255, ErrorMessage = "رمز عبور باید بیشتر از6 کاراکتر باشد", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="رمز عبور وارد شده و تکرار آن یکسان نیست")]
        [NotMapped]
        public string ConfirmPassword { get; set; }
        [Display(Name = "توضیحات کارفرما")]
        public string Description { get; set; }
        [Display(Name = "تصویر کارفرما")]
        public string Image { get; set; }
    }
}
