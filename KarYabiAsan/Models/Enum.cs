﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public static class EnumExtensions
    {
        public static string GetDisplayName(this States enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .GetName();
        }
   
    }
    public enum ModeEmployer {[Display(Name = "شخص")] Person = 1, [Display(Name = "شرکت")] Company = 2 };
    public enum ModeTaahol {[Display(Name = "ثبت نشده")] NoMatter = 0, [Display(Name = "متاهل")] Motaahel = 1, [Display(Name = "مجرد")] Mojarad = 2 };
    public enum ModeStateEshteghal {[Display(Name = "جویای کار")] JoyayKar = 1, [Display(Name = "به دنبال شغل بهتر")] BeDonbalKarBehtar = 2, [Display(Name = "شاغل")] Shaghel = 3 };
    public enum ModeHamkari {[Display(Name = "تمام وقت")] FullTime = 1, [Display(Name = "پاره وقت")] PartTime = 2, [Display(Name = "دور کاری")] Teleworking = 3 };
    public enum ModeGender {[Display(Name = "مهم نیست")] Nomatter = 3, [Display(Name = "مرد")] Man = 1, [Display(Name = "زن")] Woman = 2 }
    public enum ModeEducate {[Display(Name = "مهم نیست")] NoMatter = 1, [Display(Name = "زیر دیپلم")] ZirDiplom = 2, [Display(Name = "دیپلم")] Diplom = 3, [Display(Name = "فوق دیپلم")] FoghDiplom = 4, [Display(Name = "لیسانس")] Lisans = 5, [Display(Name = "فوق لیسانس")] FoghLisanse = 6, [Display(Name = "دکترا")] Diktora = 7, [Display(Name = "بالاتر از دکترا")] BalatarAzDokyora = 8 }
    public enum ModePayanKhedmat {[Display(Name = "مهم نیست")] NoMatter = 1, [Display(Name = "پایان خدمت")] PayanKhedmat = 2, [Display(Name = "در حال خدمت")] DarHalKhedmat = 3, [Display(Name = "معاف")] MoafAzKhedmat = 4 };
    public enum ModeMonth {[Display(Name = "فروردین")] Farvardin = 1, [Display(Name = "اردیبهشت")] Ordibehesht = 2, [Display(Name = "خرداد")] Khordad = 3, [Display(Name = "تیر")] Tir = 4, [Display(Name = "مرداد")] Mordad = 5, [Display(Name = "شهریور")] Shahrivar = 6, [Display(Name = "مهر")] Mehr = 7, [Display(Name = "آبان")] Aban = 8, [Display(Name = "آذر")] Azar = 9, [Display(Name = "دی")] Dai = 10, [Display(Name = "بهمن")] Bahman = 11, [Display(Name = "اسفند")] Esfand = 12 };
    public enum SkillsLevel {[Display(Name = "مبتدی")] Mobtadi = 1, [Display(Name = "متوسط")] Motavaset = 2, [Display(Name = "حرفه ای")] Herrfeee = 3 };
    public enum States {[Display(Name = "همه استان ها")] AllState =0, [Display(Name = "اردبیل")] ardabil = 1, [Display(Name = "اصفهان")] esfahan = 2, [Display(Name = "البرز")] alborz = 3, [Display(Name = "ایلام")] ilam = 4, [Display(Name = "آذربایجان شرقی")] azarbayjansharghi = 5, [Display(Name = "آذربایجان غربی")] Herrfeee = 6, [Display(Name = "بوشهر")] boshehr = 7, [Display(Name = "تهران")] tehran = 8, [Display(Name = "چهارمحال و بختیاری")] charmahalobakhtiari = 9, [Display(Name = "خراسان جنوبی")] khorasanjonobi = 10, [Display(Name = "خراسان رضوی")] khorasanrazavi = 11, [Display(Name = "خراسان شمالی")] khorasanshomali = 12, [Display(Name = "خوزستان")] khozestan = 13, [Display(Name = "زنجان")] zanjan = 14, [Display(Name = "سمنان")] semnan = 15, [Display(Name = "سیستان و بلوچستان")] sistanvabalochestan = 16, [Display(Name = "فارس")] fars = 17, [Display(Name = "قزوین")] qazvin = 18, [Display(Name = "قم")] qom = 19, [Display(Name = "كردستان")] kordestan = 20, [Display(Name = "کرمان")] kerman = 21, [Display(Name = "کرمانشاه")] kermanshah = 22, [Display(Name = "کهگیلویه و بویراحمد")] kogiloyevaboyerahmad = 23, [Display(Name = "گلستان")] golestan = 24, [Display(Name = "گيلان")] gialan = 25, [Display(Name = "لرستان")] lorestan = 26, [Display(Name = "مازندران")] mazandaran = 27, [Display(Name = "مرکزی")] markazi = 28, [Display(Name = "هرمزگان")] hormozgan = 29, [Display(Name = "همدان")] hamedan = 30, [Display(Name = "یزد")] yazd = 31, [Display(Name = "گیلان")] Gilan = 32 };
    public enum ModeLanquege {[Display(Name = "آلمانی")] Almani =2, [Display(Name = "ارمنی")] Armani =3, [Display(Name = "اسپانیایی")] Espaniaee =4, [Display(Name = "انگلیسی")] Englisi =5, [Display(Name = "ایتالیایی")] Italiaee =6, [Display(Name = "ترکی استانبولی")] EsatnboliTorji =7, [Display(Name = "چینی")] Chini =8, [Display(Name = "روسی")] Russi =9, [Display(Name = "ژاپنی")] Japoni =10, [Display(Name = "سوئدی")] Soedi =11, [Display(Name = "عربی")] Arabi =12, [Display(Name = "فرانسوی")] Fransavi =14,};
}