﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class HamkariRequest
    {
        [Key]
        public int Id { get; set; }
        public string EmployerId { get; set; }
        public string EmployerName { get; set; }
        public string RezomeId { get; set; }
        public DateTime RequestDate { get; set; }
        public string RequestDateShow { get; set; }
        public bool IsConfirmed { get; set; }
    }
}
