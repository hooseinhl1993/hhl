﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class Job
    {
        [Key]
        public int JobId { get; set; }

        [Display(Name = "کارفرما")]
        public int Employer { get; set; }

        [Display(Name ="عنوان شغلی")]
        [Required(ErrorMessage ="عنوان شغل را وارد کنید")]
        public string Title { get; set; }

        [Display(Name = "دسته بندی شغلی")]
        [Required(ErrorMessage = "دست بندی شل را وارد کنید")]
        public int CateGory { get; set; }

        [Display(Name = "استان")]
        public States State { get; set; }

        [Display(Name = "آدرس")]
        public string Address { get; set; }

        [Display(Name = "نوع همکاری")]
        public ModeHamkari Hamkari { get; set; }

        [Display(Name = "حداقل سابقه کار (سال)")]
        public int MinimumWorkExperience { get; set; }

        [Display(Name = "حقوق ماهیانه(تومان)")]
        public long Salery { get; set; }

        [Display(Name = "شرح موقعیت شغلی")]
        public string Decription { get; set; }

        [Display(Name = "مهارت‌های مورد نیاز")]
        public string Skills { get; set; }

        [Display(Name = "جنسیت")]
        public ModeGender Gender { get; set; }

        [Display(Name = "تحصیلات")]
        public ModeEducate Educate { get; set; }

        [Display(Name = "وضعیت پایان خدمت")]
        public ModePayanKhedmat PayanKhedmat { get; set; }
        [Display(Name = "آگهی منتشر شود")]
        public bool IsEnable { get; set; }
        public DateTime CreateDate { get; set; }
        [Display(Name = "تاریخ ایجاد")]
        public string CreateDateShow { get; set; }
        [Display(Name ="نام شرکت")]
        public string CompanyName { get; set; }
        [Display(Name = "فوری")]
        public bool IsFori { get; set; }
        [Display(Name = "تصویر شرکت")]
        public string ComponyImage { get; set; }
        public bool IsConfirm { get; set; } = false;
        [NotMapped]
        public bool IsPinned { get; set; }
        [NotMapped]
        public bool IsSend { get; set; }
    }
}
