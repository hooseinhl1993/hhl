﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class Job_Rezome
    {
        [Key]
        public long ID { get; set; }
        public string Rezome { get; set; }
        public int Job { get; set; }
    }
}
