﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class Jobs
    {

        [Key]
        public int ID { get; set; }
        public string Onvan_Sherkar_user { get; set; }
        public string City_Sherkat_User { get; set; }
        public string Phone_Sabet_User { get; set; }
        public string Phone_Hamrah_User { get; set; }
        public string Email_User { get; set; }
        public string Address_User { get; set; }
        public string NameFamily_User { get; set; }
        public string Onvan_Job_User { get; set; }
        public string Male_User { get; set; }
        public string Age_User { get; set; }
        public string Single_User { get; set; }
        public string Tahsil_User { get; set; }
        public string City_User { get; set; }
        public string Conditoin_User { get; set; }
        public string Clock_User { get; set; }
        public string Price_User { get; set; }
        public string Maharatha_User { get; set; }
        public string Info_User { get; set; }
        public DateTime NowDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public int Label { get; set; }
        public int IdUser { get; set; }
        public int IsOk { get; set; }
    }
}
