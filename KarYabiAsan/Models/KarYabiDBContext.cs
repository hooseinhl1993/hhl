﻿using System;
using KarYabiAsan.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace KarYabiAsan.Models
{
    public partial class KarYabiDBContext : IdentityDbContext
    {
        public KarYabiDBContext(DbContextOptions<KarYabiDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Employer> Employer { get; set; }
        public virtual DbSet<Job> Job { get; set; }
        public virtual DbSet<BadgedJobs> BadgedJobs { get; set; }
        public virtual DbSet<SendRezome> SendRezomes { get; set; }
        public virtual DbSet<HamkariRequest> HamkariRequests { get; set; }
        public virtual DbSet<jobsearcher> Jobsearchers { get; set; }
        public virtual DbSet<Rezome> Rezomes { get; set; }
        public virtual DbSet<JobCategory> JobCategories { get; set; }
        public virtual DbSet<Jobs> tbl_Jobs { get; set; }
        public virtual DbSet<Job_Rezome> Job_Rezomes { get; set; }
        public virtual DbSet<UsersVerifyMessage> UsersVerifies { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer("Data Source=.;Initial Catalog=KarYabiDB;Integrated Security=True;");
        //    }
        //}

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Employer>(entity =>
        //    {
        //        entity.Property(e => e.Id).HasColumnName("ID");

        //        entity.Property(e => e.Description).IsRequired();

        //        entity.Property(e => e.EmailAddress)
        //            .IsRequired()
        //            .HasMaxLength(120);

        //        entity.Property(e => e.Name)
        //            .IsRequired()
        //            .HasMaxLength(100);

        //        entity.Property(e => e.PhoneNumber)
        //            .IsRequired()
        //            .HasMaxLength(20);
        //    });

        //    OnModelCreatingPartial(modelBuilder);
        //}

        //partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
