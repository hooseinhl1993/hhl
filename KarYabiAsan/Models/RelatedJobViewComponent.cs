﻿
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    [ViewComponent(Name = "RelatedJobViewComponent")]
    public class RelatedJobViewComponent:ViewComponent
    {
        private readonly KarYabiDBContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public RelatedJobViewComponent(KarYabiDBContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;

        }

        public async Task<IViewComponentResult> InvokeAsync(int JobCategory)
        {
            var jobs = await _context.Job.Where(p => p.CateGory == JobCategory).ToListAsync();

            var user = await _userManager.GetUserAsync((System.Security.Claims.ClaimsPrincipal)User);

            if (user != null)
            {
                var IsPinned = _context.BadgedJobs.Where(p => p.UserId == user.Id).Select(p => p.JobId).ToList();
                var IsSended = _context.SendRezomes.Where(p => p.UserId == user.Id).Select(p => p.JobId).ToList();

                jobs.Where(p => IsPinned.Contains(p.JobId)).ToList().ForEach(p => p.IsPinned = true);
                jobs.Where(p => IsSended.Contains(p.JobId)).ToList().ForEach(p => p.IsSend = true);
            }
            return View("RelatedJobViewComponent", jobs.OrderByDescending(p=>p.JobId).Take(10));
        }
    }
}
