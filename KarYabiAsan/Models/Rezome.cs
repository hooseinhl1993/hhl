﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class Rezome
    {
        [Key]
        public int Id { get; set; }
        public string JobSearcheCode { get; set; }
        [Display(Name = "عنوان شغلی")]
        [Required(ErrorMessage = "عنوان شغل را وارد کنید")]
        public string OnvanShoghl { get; set; }
        [Display(Name = "نام و نام خانوادگی")]
        public string NameAndUserName { get; set; }

        [Display(Name = "وضعیت اشتغال")]
        public ModeStateEshteghal StateEshteghal { get; set; }
        // اطلاعات فردی
        [Display(Name = "شماره تماس")]
        [Required(ErrorMessage = "شماره تماس را وارد کنید")]
        public string PhoneNumber { get; set; }
        // اطلاعات فردی
        [Display(Name = "آدرس ایمیل")]
        [Required(ErrorMessage = "آدرس ایمیل را وارد کنید")]
        public string Email { get; set; }

        [Display(Name = "استان")]
        public States State { get; set; }
        [Display(Name = "شهر")]
        public string Location { get; set; }
        [Display(Name = "وضعیت تاهل")]
        public ModeTaahol Taahol { get; set; }
        [Display(Name = "سال تولد")]
        public int YearOfBrithday { get; set; }
        [Display(Name = "جنسیت")]
        public ModeGender Gender { get; set; }
        [Display(Name = "وضعیت پایان خدمت")]
        public ModePayanKhedmat PayanKhedmat { get; set; }

        // درباره‌ی من
        [Display(Name = "درباره من")]
        [Required(ErrorMessage = "توضیحاتی در مورد خود وارد کنید")]
        public string AboutMe { get; set; }
        //مهارت‌های حرفه‌ای
        [Display(Name = "مهارت‌های حرفه‌ای")]
        [Required(ErrorMessage = "مهارت‌های خود وارد کنید")]
        public string Skills { get; set; }
        [Display(Name = "تصویر کارجو")]
        public string Image { get; set; }
        [Display(Name ="فوری")]
        public bool IsFori { get; set; }
        [Display(Name = "دسته بندی رزومه")]
        [Required(ErrorMessage = "دسته بندی رزومه را وارد کنید")]
        public int RezomeCategory { get; set; }
        public string UinqCode { get; set; }
        public string _WorkExperiences { get; set; }
        /// <summary>
        /// سوایق کاری
        /// </summary>
        [NotMapped]
        public List<WorkExperience> WorkExperiences {
            get { return JsonConvert.DeserializeObject<List<WorkExperience>>(_WorkExperiences); } 
            set {_WorkExperiences= JsonConvert.SerializeObject(value);}
        }
        public string _Educationalbackgrounds { get; set; }
        /// <summary>
        /// سابقه تحصیلی
        /// </summary>
        [NotMapped]
        public List<Educationalbackground> Educationalbackgrounds
        {
            get { return JsonConvert.DeserializeObject<List<Educationalbackground>>(_Educationalbackgrounds); }
            set { _Educationalbackgrounds = JsonConvert.SerializeObject(value); }
        }
        public string _Lanquege { get; set; }
        /// <summary>
        /// زبان های 
        /// </summary>
        [NotMapped]

        public List<Lanquege> Lanquege {
            get { return JsonConvert.DeserializeObject<List<Lanquege>>(_Lanquege); }
            set { _Lanquege = JsonConvert.SerializeObject(value); }
        }
        [NotMapped]
        public bool IsEdited { get; set; } = false;
        public bool IsConfirm { get; set; } = false;
        [NotMapped]
        public bool requesSended { get; set; } = false;
        [NotMapped]
        public bool requesConfirmed { get; set; } = false;
    }
}
