﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    [ViewComponent(Name = "SearchViewComponent")]
    public class SearchViewComponent : ViewComponent
    {
        private readonly KarYabiDBContext _context;

        public SearchViewComponent(KarYabiDBContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(string Action)
        {
            ViewBag.Action = Action;
            return View("SearchViewComponent");
        }
    }
}
