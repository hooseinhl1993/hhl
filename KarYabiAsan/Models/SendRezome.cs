﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class SendRezome
    {
        [Key]
        public int ID { get; set; }
        public int JobId { get; set; }
        public string UserId { get; set; }
    }
}
