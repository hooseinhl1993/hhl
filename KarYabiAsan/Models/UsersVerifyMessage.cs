﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class UsersVerifyMessage
    {
        public long ID { get; set; }
        public string PhoneNumber { get; set; }
        public int Code { get; set; }
        public DateTime dateTime { get; set; }
    }
}
