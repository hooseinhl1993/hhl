﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models.ViewModels
{
    public class EployerJobViewModel
    {
        public Employer Employer { get; set; }
        public IEnumerable<Job> Jobs { get; set; }
        public IEnumerable<JobCategory> JobCategories { get; set; }
    }
}
