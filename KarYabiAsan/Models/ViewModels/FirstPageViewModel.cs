﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models.ViewModels
{
    public class FirstPageViewModel
    {
        public IEnumerable<Job> ForiEmrozJobs { get; set; }
        public IEnumerable<Job> LastJobs { get; set; }
        public IEnumerable<Rezome> ForiRezome { get; set; }
        public IEnumerable<Rezome> LastRezomes { get; set; }
        public IEnumerable<jobsearcher> Jobsearchers { get; set; }
        public IEnumerable<JobCategory> Categories { get; set; }
    }
}
