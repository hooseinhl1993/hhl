﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models.ViewModels
{
    public class JobSearcherRezomeViewModel
    {
        public jobsearcher jobsearcher { get; set; }
        public Rezome Rezome { get; set; }
        public List<JobCategory> jobCategories { get; set; }
    }
}
