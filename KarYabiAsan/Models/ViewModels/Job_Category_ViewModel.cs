﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models.ViewModels
{
    public class Job_Category_ViewModel
    {
        public Job Job { get; set; }
        public Employer Employer { get; set; }
        public IEnumerable<JobCategory> JobCategory { get; set; }
    }
}
