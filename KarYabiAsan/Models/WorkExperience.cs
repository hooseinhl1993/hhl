﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class WorkExperience
    {
        public int ID { get; set; }
        public string JobTitle { get; set; }

        public string CompanyName { get; set; }

        public ModeMonth StartMonth { get; set; }

        public int StartYear { get; set; }

        public ModeMonth EndMonth { get; set; }

        public int EndYear { get; set; }

        public bool StillInJob { get; set; }

        public string Description { get; set; }
    }
}
