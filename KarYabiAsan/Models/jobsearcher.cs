﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KarYabiAsan.Models
{
    public class jobsearcher
    {
        [Key]
        public int Id { get; set; }
        public string UserID { get; set; }
        public IdentityUser User { get; set; }
        public string Token { get; set; } = "";
        [Display(Name = "نام و نام خانوادگی")]
        public string Name { get; set; } = "";

        [Display(Name = "شماره همراه")]
        [Required(ErrorMessage = "شماره همراه را وارد کنید")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; } = "";
        [Display(Name = "آدرس ایمیل")]
        [Required(ErrorMessage = "آدرس ایمیل را وارد کنید")]
        [DataType(DataType.EmailAddress, ErrorMessage = "مقدار ورودی باید ایمیل باشد")]
        public string Email { get; set; }
        public string UinqCode { get; set; }
        [NotMapped]
        public string Password { get; set; }
        [NotMapped]
        public string OldPassword { get; set; }
    }
}
