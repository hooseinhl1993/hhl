using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KarYabiAsan.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace KarYabiAsan
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //services.AddDbContext<Models.KarYabiDBContext>(c => c.UseSqlServer("Data Source=.;Initial Catalog=NewKaryabi;Integrated Security=True;"));
            //services.AddDbContext<KarYabiAsanContext>(c => c.UseSqlServer("Data Source=.;Initial Catalog=KarYabiDB;Integrated Security=True;"));

            services.AddDbContext<KarYabiDBContext>(options =>
              options.UseSqlServer(
                  Configuration.GetConnectionString("KarYabiAsanContextConnection")));
            //services.AddDbContext<Models.KarYabiDBContext>(c => c.UseSqlServer("Data Source = 185.55.227.84;Initial Catalog=telquran_Jobs; User ID = telquran_JobUser; Password = J0bU$er@@"));
            services.AddSession(Option =>
            {
                Option.IdleTimeout = TimeSpan.FromDays(22);
                Option.Cookie.HttpOnly = true;
            });

            services.AddIdentity<IdentityUser, IdentityRole>(
                         options =>
                         {
                             //options.SignIn.RequireConfirmedAccount = true;
                             //options.Password.RequireDigit = false;
                             //options.Password.RequireLowercase = false;
                             //options.Password.RequiredLength = 3;
                             //options.Password.RequireNonAlphanumeric= false;
                             //options.Password.RequireUppercase= false;
                             //options.SignIn.RequireConfirmedEmail = false;
                             //options.SignIn.RequireConfirmedPhoneNumber = true;
                             //options.SignIn.RequireConfirmedAccount = false;
                             //options.User.AllowedUserNameCharacters = null;
                             options.SignIn.RequireConfirmedEmail = false;
                             options.SignIn.RequireConfirmedPhoneNumber = false;
                             options.SignIn.RequireConfirmedAccount = false;
                             options.Password.RequiredLength = 6;
                             options.Password.RequiredUniqueChars = 6;
                             options.Password.RequireLowercase = false;
                             options.Password.RequireNonAlphanumeric = false;
                             options.Password.RequireUppercase = false;
                             options.User.AllowedUserNameCharacters = null;
                         }


                         )
                .AddEntityFrameworkStores<KarYabiDBContext>()
                .AddDefaultUI()
                .AddDefaultTokenProviders();


            services.AddControllersWithViews();
            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseSession();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });


        }//test commit
    }
}
