﻿using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using GlobalClass;
namespace SMS
{
    public class KasBarg
    {
        static string ApiUrl
        {
            get
            {
                return "http://api.smsapp.ir/v2/send/verify";//آدرس پایه وب سرویس;اینترانت
            }
        }

        static string apikey = "VXwM9xxeC6x8WnpRKZ8uoPpyafNRWRadOsDWMJoub9Y";
        /// <summary>
        /// ارسال اس ام اس
        /// </summary>
        /// <param name="Message">مشخصات پیام ارسالی</param>
        /// <returns></returns>
        public static async Task<bool> Send(string Token, MessageVerify Message)
        {
            //"{\"receptor\":\"" + message.receptor + "\",\"param1\":\"" + message.param1 + "\",\"param2\":\"" + message.param2 + "\",\"param3\":\"" + message.param3 + "\",\"type\":" + message.type + ",\"template\":\"" + message.template + "\"}";
            string MessageString = await Utility.JSON.ToJson(Message);

            var R = await PostHttpClient.SendAsync(ApiUrl, MessageString, apikey);
            //{"result":"success","messageids":2016461535}
            //{ "result":"error","messageids":8}
            //{ "result":"error","message":"invalid template"}
            if (R.Status != System.Net.HttpStatusCode.OK)
            {
                return false;
            }

            string Responce = R.Content;
            var DATA = JObject.Parse(Responce);
            string MessageJson = await Utility.JSON.ToJson(Message);

            bool State = false;

            try
            {
                string result = (string)DATA["result"];
                if (result == "success")
                {
                    State = true;
                }
            }
            catch
            {
            }

            ////ذخیره لاگ اس ام اس
            //await LogDB.SMS.Log(Token, MessageJson, Responce, State);

            return true;
        }

        /// <summary>
        /// ارسال کد تائیدیه اپلیکیشن دسکتاپ و انجمن مجازی
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="Receptor"></param>
        /// <param name="Code"></param>
        /// <returns></returns>
        public static async Task<bool> SendCodeVerify(string Token, string Receptor, string Code)
        {
            MessageVerify Message = new MessageVerify()
            {
                receptor = Receptor,
                param1 = Code,
                template = "VerifayCodeJob",
                type = 1
            };

            var R = await Send(Token, Message);

            return R;
        }
        /// <summary>
        /// ارسال کد سازمانی
        /// منادی
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="Receptor"></param>
        /// <param name="Code"></param>
        /// <returns></returns>
        public static async Task<bool> SendCodeSazmani(string Token, string Receptor, string Code)
        {
            //کد سازمانی جهت ثبت نام کاربران شما در اپ منادی
            //% param1 %
            //belquran.com

            MessageVerify Message = new MessageVerify()
            {
                receptor = Receptor,
                param1 = Code,
                template = "CodeSazmani",
                type = 1
            };

            var R = await Send(Token, Message);

            return R;
        }


        public static async Task<bool> ChangePassword(string Token, string Receptor, string Code)
        {
            MessageVerify Message = new MessageVerify()
            {
                receptor = Receptor,
                param1 = Code,
                template = "ChangePassword",
                type = 1
            };

            var R = await Send(Token, Message);

            return R;
        }
        /// <summary>
        /// وقتی سرویسی قطع میشود پیامک دهد
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="Receptor"></param>
        /// <param name="ServiceName"></param>
        /// <returns></returns>
        public static async Task<bool> StopService(string Token, string Receptor, string ServiceName)
        {
            MessageVerify Message = new MessageVerify()
            {
                receptor = Receptor,
                param1 = ServiceName,
                template = "StopService",
                type = 1
            };

            var R = await Send(Token, Message);

            return R;
        }
        /// <summary>
        /// ارسال کد ثبت نام و لاگین دختران کتاب
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="Receptor"></param>
        /// <param name="ServiceName"></param>
        /// <returns></returns>
        public static async Task<bool> SendCodeVerifyDokhtaranKetab(string Token, string Receptor, string Code)
        {
            MessageVerify Message = new MessageVerify()
            {
                receptor = Receptor,
                param1 = Code,
                template = "DokhtaranKetabValidationMobile",
                type = 1
            };

            var R = await Send(Token, Message);

            return R;
        }
    }
}
